package io.javabrains.ratingsdataservice.resources;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.javabrains.ratingsdataservice.model.Rating;
import io.javabrains.ratingsdataservice.model.UserRating;

@RestController
@RequestMapping("/ratingsdata")
@RefreshScope
public class RatingsResource {

	@Value("${movie.id}")
	List<String> movieList;

	@RequestMapping("/movies/{movieId}")
	public Rating getMovieRating(@PathVariable("movieId") String movieId) {
		return new Rating(movieId, 4);
	}

	@RequestMapping("/user/{userId}")
	public UserRating getUserRatings(@PathVariable("userId") String userId) {
		UserRating userRating = new UserRating();
		userRating.setUserId(userId);
		List<Rating> ratingList = new ArrayList<>();
		for (String movieId : movieList) {
			System.out.println("Movie ID from rating data service: " + movieId);
			ratingList.add(new Rating(movieId, 4));
		}
		userRating.setRatings(ratingList);
		return userRating;

	}

}
