package io.javabrains.ratingsdataservice.model;

public class Rating {

    private String movieId;
    private int rating;

    public Rating(String movieId, int rating) {
        this.movieId = movieId;
        this.rating = rating;
    }

    public String getMovieId() {
        return movieId;
    }

    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Rating [movieId=").append(movieId).append(", rating=").append(rating).append("]");
		return builder.toString();
	}
    
    
    
}
